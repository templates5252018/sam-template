#!/bin/bash

YELLOW='\033[1;33m'
NC='\033[0m' # No Color
UUID=$$
SOURCE="./cloudformations/"
PROJECT=sam-example
ENVIRONMENT=develop
STACK=$ENVIRONMENT-$PROJECT-security
BUCKET=$ENVIRONMENT-$PROJECT-deploys
AWS_PROFILE=nnnp
AWS_REGION="us-east-1" #IF REGION EQUALS TO us-east-1 THE PARAMETER "LocationConstraint" IS NOT REQUIRED

echo -e "${YELLOW} Creating Bucket... ${AWS_REGION}"
echo -e " ================================================= ${NC}"
aws s3api create-bucket --bucket $BUCKET  --profile $AWS_PROFILE --region $AWS_REGION #--create-bucket-configuration LocationConstraint=$REGION
echo -e "${YELLOW} Validating local SAM Template..."
echo -e " ================================${NC}"
sam validate --profile $AWS_PROFILE --template "${SOURCE}/security.yaml"
sam package --template-file "${SOURCE}/security.yaml" --output-template-file "security_$UUID.yaml" --s3-bucket $BUCKET --profile $AWS_PROFILE --region $AWS_REGION 
echo -e "${YELLOW} Deploy"
echo -e " ================================================= ${NC}"
sam deploy --template-file "security_$UUID.yaml" --stack-name $STACK --s3-bucket $BUCKET --tags Project=$PROJECT --capabilities CAPABILITY_NAMED_IAM --parameter-overrides Environment=$ENVIRONMENT --profile $AWS_PROFILE --region $AWS_REGION
rm "security_$UUID.yaml"
