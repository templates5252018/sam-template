#!/bin/bash
YELLOW='\033[1;33m'
NC='\033[0m' # No Color
# VARIABLES STANDAR
ENVIRONMENT=develop #THIS WORK FINE IF WE USE SAM IN LOCAL. IN PIPELINE IS NOT NEED
PROJECT=sam-example
STACK=$ENVIRONMENT-$PROJECT
BUCKET=$ENVIRONMENT-$PROJECT-deploys
AWS_PROFILE=nnnp
REGION="us-east-1" #IF REGION EQUALS TO us-east-1 THE PARAMETER "LocationConstraint" IS NOT REQUIRED

echo -e "${YELLOW} Creating Bucket..."
echo -e " ================================${NC}"
aws s3api create-bucket --bucket $BUCKET --region $REGION --profile $AWS_PROFILE #--create-bucket-configuration LocationConstraint=$REGION

echo -e "${YELLOW} Validating local SAM Template..."
echo -e " ================================${NC}"
sam validate --profile $AWS_PROFILE --template "template.yaml"

echo -e "${YELLOW} Building local SAM App..."
echo -e " =========================${NC}"
sam build --profile $AWS_PROFILE --region $REGION -t "template.yaml"

echo -e "${YELLOW} Package"
echo -e " ================================================= ${NC}"
sam package --profile $AWS_PROFILE --region $REGION --template-file .aws-sam/build/template.yaml --output-template-file .aws-sam/build/packaged-template.yaml --s3-bucket $BUCKET

echo -e "${YELLOW} Deploy"
echo -e " ================================================= ${NC}"
sam deploy --no-fail-on-empty-changeset -t .aws-sam/build/packaged-template.yaml --s3-bucket $BUCKET --profile $AWS_PROFILE --capabilities CAPABILITY_NAMED_IAM --region $REGION --stack-name $STACK --parameter-overrides Environment=$ENVIRONMENT
