const app = require('../../lambdas/getCategories/app');
const mockQuery = require('./mock/query.json');
const { getCategories, getSubcategories } = require('../../lambdas/getCategories/queries/queries');

jest.mock('../../lambdas/getCategories/queries/queries');

describe('Get Categories', () => {
  it('200', async () => {
    getCategories.mockReturnValue(mockQuery.categoriesWithData);
    getSubcategories.mockReturnValue(mockQuery.subCategoriesWithData);

    const result = await app.handler({ body: {} });
    expect(result).toBeDefined();
    
    const body = JSON.parse(result.body);
    console.info('result', body);

    expect(result.statusCode).toEqual(200);
    expect(body.length).toBeGreaterThan(0);
    expect(body[0].id).toBe(2);
  });
  
  it('204', async () => {
    getCategories.mockReturnValue(mockQuery.categoriesWithoutData);
    getSubcategories.mockReturnValue(mockQuery.subCategoriesWithoutData);

    const result = await app.handler({ body: {} });
    expect(result).toBeDefined();
    
    const body = JSON.parse(result.body);
    console.info('result', body);

    expect(result.statusCode).toEqual(204);
  });

  it('500', async () => {
    getCategories.mockReturnValue(Error('Fail db'));
    const result = await app.handler();
    const body = JSON.parse(result.body);
    
    expect(result).toBeDefined();
    expect(result.statusCode).toEqual(500);
    expect(body.error.code).toEqual('internal_error');
  });
});