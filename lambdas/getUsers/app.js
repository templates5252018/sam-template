const layersPath = '/opt/nodejs';
const { log, responseFactory } = require(`${process.env.LOCAL ? '../../layers/utils' : layersPath}/utils`);
const { getList } = require(`${process.env.LOCAL ? '../../layers/persistance' : layersPath}/persistance`);

exports.handler = async event => {
  try {
    log.info('EVENT', { event });

    const users = getList(`${process.env.NODE_ENV}-Users`);

    return responseFactory.response(users);
  } catch (error) {
    return responseFactory.error(error);
  }
}