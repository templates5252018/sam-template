const AWS = require('aws-sdk');
const docClient = new AWS.DynamoDB.DocumentClient();

module.exports = {
  getList: async TableName => {
    let scanResults = [];
    let items;

    do {
      items = await docClient.scan({ TableName }).promise();
      items.Items.forEach(item => scanResults.push(item));
    } while (typeof items.LastEvaluatedKey != "undefined");

    return scanResults;
  }
}