const log = require('lambda-log');

module.exports = {
  log: {
    info: (msg, event) => {
      log.info(msg, event);
    },
    error: (msg, event) => {
      log.error(msg, event);
    }
  }
}