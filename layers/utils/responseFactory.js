const lodash = require('lodash');
const { log } = require("./log");
const { getDictionary } = require("./dictionaries");

const dictionaryError = getDictionary('error');

function error(error) {
  let errorKey;

  if (typeof error === 'string') {
    errorKey = error;
  } else if (error.errorMessage) {
    errorKey = error.errorMessage;
  } else if (error.stack) {
    errorKey = error.toString();
    error = { stack: error.stack };
  }

  log.error('[RESPONSE ERROR]', error);
  let errorResponse = dictionaryError.getValue(errorKey);

  if (!errorResponse) {
    errorResponse = dictionaryError.getValue('default');
  }

  return this.response(errorResponse.body, errorResponse.statusCode);
}

function response(data, status) {
  const response = {
    headers: { 'Access-Control-Allow-Origin': '*', 'Content-Type': 'application/json' },
    statusCode: status ? status : lodash.isEmpty(data) ? 204 : 200,
    isBase64Encoded: false,
    body: JSON.stringify(data)
  }

  log.info('[RESPONSE]', response);
  return response;
}

module.exports = {
  responseFactory: {
    error,
    response
  }
}
