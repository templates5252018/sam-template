const Dictionary = require('./dictionaries/dictionaryService');

module.exports = {
  getDictionary: dictionary => {
    return new Dictionary(dictionary);
  }
}
