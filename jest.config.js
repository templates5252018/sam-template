module.exports = {
  'injectGlobals': true,
  'verbose': true,
  'coverageThreshold': {
    'global': {
      'branches': 10,
      'functions': 10,
      'lines': 10,
      'statements': -5000
    }
  },
  'coverageReporters': [
    'text'
  ],
  'collectCoverage': true,
  'collectCoverageFrom': [
    '**/lambdas/**/*.js',
    '!**/lambdas/**/queries/*.js'
  ],
  'testMatch': ['**/tests/**/*.test.{js,jsx}'],
  'setupFiles': ['./tests/setEnvVars.js'],
  'clearMocks': true
}